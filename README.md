# FaitMaison.co Blog Repository

This will house every blog entry to appear on www.faitmaison.co

Each entry should be realised as a separate .md (Markdown) file.

The beginning of each file can include some metadata. e.g.

	title: Static Blog Engine in Node.js
	author: Will
	date: 2012-06-18
	category: Setting up the blog
	tags: node.js, web, blog
	