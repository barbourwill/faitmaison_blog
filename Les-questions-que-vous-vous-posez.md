title: Les questions que vous vous posez
date: 2013-01-18

####Qu'est-ce que je peux cuisiner sur Faitmaison.co ?
<hr/>

Les gourmands inscrits sur Faitmaison.co sont en quête de nouvelles saveurs, pour que ton petit plat soit un succès il faut que tu proposes une recette originale qui saura les séduire. La tartiflette ou les crêpes sont à proscrire, mais le tajine et les plats indiens sont de bonnes idées pour commencer :)

Et pourquoi pas faire un petit dessert en plus du plat salé?
<br/><br/>

####Quelle quantité est-ce que je dois cuisiner?
<hr/>

Cuisiner doit rester un plaisir ! Nous vous conseillons des petites quantités pour commencer. Proposez de 2 à 4 parts pour les premières fois et voyez ensuite si vous souhaitez cuisiner plus.

D'ailleurs nous vous recommandons de ne pas cuisiner spécialement pour faitmaison.co mais de faire quelques parts supplémentaires du repas que vous préparez le dimanche.

<br/>

####Quel prix demander?
<hr/>
Le prix proposer doit refléter la contribution aux frais que vous attendez (ingrédients, déplacement pour faire les courses,...). La fourchette constatée est entre 5 et 8€ - Mais bien sûr cela varie en fonction de chaque plat, à vous de faire le calcul :)

<br/>

####Je ne suis pas à Grenoble, est-ce que je peux m'inscrire?
<hr/>

Oui! Si pour l'instant Faitmaison.co n'est disponible qu'à Grenoble, d'ici quelques semaines,  cela devrait changer :)

<br/>

####Comment je sais si quelqu'un a commandé?
<hr/>

Lorsqu'un gourmand est séduit par votre plat vous recevez un e-mail pour vous en informer

<br/>

####Jusqu'à quand les gourmands peuvent-ils commander?
<hr/>

Les gourmands peuvent commander jusqu'à 48h avant que vous ne cuisiniez, mais si vous faites vos courses plus tôt, envoyez nous un message et nous ferons le nécessaire pour éviter que vous ne soyez surchargé !

<br/>

####Des questions restent sans réponse?
<hr/>

Nous sommes à votre écoute, écrivez-nous: maxime [a] faitmaison.co
