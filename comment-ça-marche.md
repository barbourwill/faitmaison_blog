title: Comment ça marche Faitmaison.co?
date: 2013-01-17

##Comment ça marche Faitmaison.co?
<br/>
Vous êtes nombreux à nous le demander régulièrement alors voici quelques explications !

Tout d'abord Faitmaison.co ça marche grâce au plaisir de la cuisine ! Nous souhaitons donner aux Cuistos amateurs mais passionnés l'opportunité de faire découvrir leur délicieuse cuisine à des gourmands en quête de nouvelles saveurs!

Vous êtes tenté ? Alors voici plus concrètement le fonctionnement :
<br/>


###1 - S'inscrire et proposer un plat - J-7
<br/>
[![Proposer un plat](http://i.imgur.com/xrON64I.png)](https://docs.google.com/a/faitmaison.co/forms/d/1PBLsFb3p4LAbTBltsA7FP6tGtTK9qirgNKAjWSC12IU/viewform)

Lorsque vous êtes Cuistos et que vous voulez faire frémir de nouvelles papilles, vous devez vous rendre sur le site et inscrire votre adresse e-mail.

Vous recevez alors un message contenant un lien qui vous permet de proposer un petit plat, il s'agit d'un questionnaire nous permettant de mettre en ligne vos plat. Ils sont mis en ligne le mardi pour le dimanche, pensez donc à l'inscrire à l'avance !

Vous pouvez d'ailleurs proposer en cliquant ici: [Proposer un plat](https://docs.google.com/a/faitmaison.co/forms/d/1PBLsFb3p4LAbTBltsA7FP6tGtTK9qirgNKAjWSC12IU/viewform)

Pour l'instant Faitmaison vous permet de proposer des plats seulement le dimanche. Cela nous permet de mieux parler de vous aux gourmands :)
<br/><br/>
    
####2 - Votre plat est en ligne - J-5
<br/>
[![Credit je-papote.com](http://4.bp.blogspot.com/-6FHi2HD2kC0/UtkyrJipOJI/AAAAAAAAADs/A79YTQiO428/s1600/platenligne.tiff)](http://www.je-papote.com)
<br/><br/>
Vous êtes désormais sous les projecteurs et c'est à vous de jouer ! Nous allons parler de vous à nos gourmands mais vous devez aussi en parler aux gourmands autour de vous, pour qu'ils en parlent aux gourmands autour d'eux, etc... :)

Nous mettons vos petits plats en ligne le mardi pour des plats qui seront cuisinés le dimanche, vous avez donc une semaine pour en parler autour de vous. Et les gourmands ont près d'une semaine pour commander.

Pendant ce temps nous vous apportons les boîtes alimentaires (à pied, en tram ou par la poste) afin que vous puissiez servir vos gourmands dans les meilleures conditions qui soient !
<br/><br/>

####3 - Vos premières commandes - jusqu'à J-2
<br/>
![](http://i.imgur.com/GoLSV0p.png)
<br/>
<br/>
A chaque fois que vous avez conquis un gourmand, vous recevez un e-mail avec ses coordonnées. C'est à vous de le contacter pour avant le jour J pour répondre à ses questions et lui fixer un lieu rendez-vous (à votre adresse ou à l'arrêt de tram le plus proche par exemple !)

Lorsque votre plat est prêt et que le gourmand est arrivé, n'hésitez pas à échanger quelques mots ou quelques astuces cuisines :)

<br/>

###Prêt à devenir un Cuisto hors pairs? 

<br/>

Alors lancez-vous : [http://faitmaison.co](http://faitmaison.co)

Proposez un plat: [cliquez ici](https://docs.google.com/a/faitmaison.co/forms/d/1PBLsFb3p4LAbTBltsA7FP6tGtTK9qirgNKAjWSC12IU/viewform)

Encore des questions? Consultez cet article: ["les questions que vous vous posez"](http://faitmaison.co/blog/Les-questions-que-vous-vous-posez.md)

à bientôt pour de belles aventures culinaires !
<br/><br/>
L'équipe de Faitmaison.co
